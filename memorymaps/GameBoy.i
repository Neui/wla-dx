; GameBoy memory map
; By Ville Helin <vhelin@iki.fi>

.MEMORYMAP
    DEFAULTSLOT 0
    SLOTSIZE $4000
    SLOT 0 START $0000
    SLOT 1 START $4000
.ENDME

