memorymaps
==========

A memory map tells WLA DX how the memory looks like. Because all object files
and libraries should use the same memory, these are a good start to use.

If you've made a memory map or updated one, feel free to contribute. Please
note that everyone would then be able to use the memory map files freely.

### SNES

You have to define `ROM_TYPE` to either `"LOROM"` or `"HIROM"` and you have
to define either `KBIT`, `MBIT` or `BANK_COUNT`.

### GB

Nothing to be detailed, but the `.ROMBANKMAP` is missing.

### SMS/GG

Nothing to be detailed.

